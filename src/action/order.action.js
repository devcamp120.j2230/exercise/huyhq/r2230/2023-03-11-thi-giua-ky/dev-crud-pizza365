import axios from "axios";
import { ORDERS_CREATE_NEW, ORDERS_DELETE_BY_ID, ORDERS_FETCH_ERROR, ORDERS_FETCH_PENDING, ORDERS_FETCH_SUCCESS, ORDERS_PAGE_CHANGE, ORDERS_UPDATE_BY_ID } from "../constants/order.constant";

const url = "http://203.171.20.210:8080/devcamp-pizza365/orders";

const axiosCall = async (url, body) => {
    const response = await axios(url, body);
    return response.data;
};

//get All Order
export const getAllOrder = (page, limit) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        });

        axiosCall(url)
            .then(result => {
                return dispatch({
                    type: ORDERS_FETCH_SUCCESS,
                    totalOrder: result.length,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDERS_FETCH_ERROR,
                    error
                })
            });

    }
}
//create new Order
export const createNewOrder = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        });

        var config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axiosCall(url, config)
            .then(result => {
                return dispatch({
                    type: ORDERS_CREATE_NEW,
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDERS_FETCH_ERROR,
                    error
                })
            });

    }
}
//update order by Id
export const updateOrderByCode = (orderId, data)=>{
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        });

        var config = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axiosCall(url+"/"+orderId, config)
            .then(result => {
                console.log(result);
                return dispatch({
                    type: ORDERS_UPDATE_BY_ID,
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDERS_FETCH_ERROR,
                    error
                })
            });

    }
};
//delete order by id
export const deleteOrderById = (orderId)=>{
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        });

        var config = {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			},
		};

        axiosCall(url+"/"+orderId, config)
            .then(result => {
                return dispatch({
                    type: ORDERS_DELETE_BY_ID,
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDERS_FETCH_ERROR,
                    error
                })
            });

    }
};

//change Pagination
export const pageChangePagination = (page) => {
    return {
        type: ORDERS_PAGE_CHANGE,
        page: page
    }
};