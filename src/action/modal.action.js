import { MODAL_ADD_OPEN, MODAL_DELETE_OPEN, MODAL_UPDATE_OPEN } from "../constants/order.constant";

//open model add new order
export const modalAddOpenAction = () => {
    return {
        type: MODAL_ADD_OPEN,
        status: true
    }
};
//close model add new order
export const modalAddCloseAction = () => {
    return {
        type: MODAL_ADD_OPEN,
        status: false
    }
};
//open model update order by id
export const modalUpdateOpenAction = () => {
    return {
        type: MODAL_UPDATE_OPEN,
        status: true,
    }
};
//close model update order by id
export const modalUpdateCloseAction = () => {
    return {
        type: MODAL_UPDATE_OPEN,
        status: false,
    }
};
//open model delete order by id
export const modalDeleteOpenAction = () => {
    return {
        type: MODAL_DELETE_OPEN,
        status: true,
    }
};
//close model delete order by id
export const modalDeleteCloseAction = () => {
    return {
        type: MODAL_DELETE_OPEN,
        status: false,
    }
}