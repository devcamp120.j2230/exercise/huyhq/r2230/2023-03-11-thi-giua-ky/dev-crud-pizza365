import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import orderReducer from "../reducers/order.reducer";


const rootReducer = combineReducers({
    orderReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;