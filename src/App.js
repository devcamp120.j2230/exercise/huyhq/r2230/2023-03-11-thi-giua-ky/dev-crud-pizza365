import OrderComponent from "./components/OrderComponent";

function App() {
  return (
    <div>
      <OrderComponent/>
    </div>
  );
}

export default App;
