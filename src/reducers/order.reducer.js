import { MODAL_ADD_OPEN, MODAL_DELETE_OPEN, MODAL_UPDATE_OPEN, ORDERS_CREATE_NEW, ORDERS_DELETE_BY_ID, ORDERS_FETCH_ERROR, ORDERS_FETCH_PENDING, ORDERS_FETCH_SUCCESS, ORDERS_PAGE_CHANGE, ORDERS_UPDATE_BY_ID } from "../constants/order.constant";

const orderTask = {
    orders: [],
    pending: false,
    currentPage: 1,
    limit: 10,
    numPage: 0,
    actionChangeStatus: false,
    modalAddOpen: false,
    modalUpdateOpen: false,
    modalDeleteOpen: false,
    alert: {
        status: "",
        message: "",
    }
}

const orderReducer = (state = orderTask, action) => {
    switch (action.type) {
        case ORDERS_FETCH_PENDING:
            state.pending = true;
            break;
        case ORDERS_FETCH_SUCCESS:
            state.actionChangeStatus = false;
            state.pending = false;
            state.numPage = Math.ceil(action.totalOrder / state.limit);
            state.orders = action.data.slice((state.currentPage - 1) * state.limit, (state.currentPage * state.limit));
            break;
        case ORDERS_FETCH_ERROR:
            state.alert.status = false;
            state.alert.message = "Có lỗi xảy ra, hãy thử lại!"
            break;
        case ORDERS_PAGE_CHANGE:
            state.alert.status = "";
            state.currentPage = action.page;
            break;
        case MODAL_ADD_OPEN:
            state.modalAddOpen = action.status;
            break;
        case MODAL_UPDATE_OPEN:
            state.modalUpdateOpen = action.status;
            break;
        case MODAL_DELETE_OPEN:
            state.modalDeleteOpen = action.status;
            break;
        case ORDERS_CREATE_NEW:
            state.actionChangeStatus = true;
            state.alert.status = true;
            state.alert.message = "Tạo mới Order thành công!"
            break;
        case ORDERS_UPDATE_BY_ID:
            state.actionChangeStatus = true;
            state.alert.status = true;
            state.alert.message = "Sửa Order thành công!"
            break;
        case ORDERS_DELETE_BY_ID:
            state.actionChangeStatus = true;
            state.alert.status = true;
            state.alert.message = "Xóa Order thành công!"
            break;
        default:
            break;
    }
    return { ...state };
}

export default orderReducer;