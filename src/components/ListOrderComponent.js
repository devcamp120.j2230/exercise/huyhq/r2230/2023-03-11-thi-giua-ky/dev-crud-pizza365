import { Container, CircularProgress, Grid, TableContainer, TableHead, TableRow, TableCell, TableBody, Button, Paper, Table, Pagination } from "@mui/material"
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalDeleteOpenAction, modalUpdateOpenAction } from "../action/modal.action";
import { pageChangePagination } from "../action/order.action";
import DeleteModal from "./DeleteOrderModal";
import UpdateOrderModal from "./UpdateOrderModal";

const ListOrderComponent = () => {
    const [orderId, setOrderId] = useState("");
    const [orderData, setOrderData] = useState("");

    const dispatch = useDispatch();

    const { orders, pending, numPage, currentPage } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });

    //khi chọn button edit
    const onClickButtonEdit = async (e) => {
        //load Api oder by order code
        const responseOrder = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders/" + e.target.dataset.code)
        const orderJson = await responseOrder.json();

        setOrderData(orderJson)
        dispatch(modalUpdateOpenAction());

    };

    //khi chọn button delete
    const onClickButtonDelete = (e) => {
        setOrderId(e.target.dataset.id);
        dispatch(modalDeleteOpenAction());
    };

    //khi ấn phân trang
    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    };

    return (
        <Container>
            {
                pending
                    ?
                    <Grid container justifyContent='center'>
                        <CircularProgress />
                    </Grid>
                    :
                    <Grid container justifyContent='center'>
                        <Grid item xs={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow sx={{ fontWeight: "700", background: "grey" }}>
                                            <TableCell align="left">OrderCode</TableCell>
                                            <TableCell align="right">Kích cỡ</TableCell>
                                            <TableCell align="right">Loại Pizza</TableCell>
                                            <TableCell align="right">Nước</TableCell>
                                            <TableCell align="right">Thành tiền</TableCell>
                                            <TableCell align="right">Họ tên</TableCell>
                                            <TableCell align="right">Số điện thoại</TableCell>
                                            <TableCell align="right">Trạng thái</TableCell>
                                            <TableCell align="center">Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {orders.map((row) => {
                                            return <TableRow
                                                key={row.id}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {row.orderCode}
                                                </TableCell>
                                                <TableCell align="right">{row.kichCo}</TableCell>
                                                <TableCell align="right">{row.loaiPizza}</TableCell>
                                                <TableCell align="right">{row.idLoaiNuocUong}</TableCell>
                                                <TableCell align="right">{row.thanhTien}</TableCell>
                                                <TableCell align="right">{row.hoTen}</TableCell>
                                                <TableCell align="right">{row.soDienThoai}</TableCell>
                                                <TableCell align="right">{row.trangThai}</TableCell>
                                                <TableCell align="center">
                                                    <Button variant="contained" color="info" sx={{ marginRight: "3px" }} onClick={onClickButtonEdit} data-code={row.orderCode}>Sửa</Button>
                                                    <Button variant="contained" color="error" onClick={onClickButtonDelete} data-id={row.id}>Xóa</Button>
                                                </TableCell>
                                            </TableRow>
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item lg={12} md={12} sm={12} xs={12} mt={5}>
                            <Pagination count={numPage} page={currentPage} onChange={onChangePagination} />
                        </Grid>
                    </Grid>
            }
            <DeleteModal orderId={orderId} />
            {
                orderData !== ""
                    ? <UpdateOrderModal orderData={orderData} />
                    : <></>
            }
        </Container>
    )
}

export default ListOrderComponent;