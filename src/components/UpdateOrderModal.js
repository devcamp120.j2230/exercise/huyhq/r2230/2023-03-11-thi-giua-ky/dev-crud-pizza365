import { Button, Modal, Box, Grid, TextField, InputLabel, MenuItem, FormControl, Select, Typography, Alert } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalUpdateCloseAction, modalUpdateOpenAction } from "../action/modal.action";
import { updateOrderByCode } from "../action/order.action";
import { styleModal } from "../styles/Modal.style";

const UpdateOrderModal = (props) => {
    const orderData = props.orderData;

    const [validate, setValidate] = useState(false);
    const [message, setMessage] = useState("Nhập thông tin.");

    const [trangThai, setTrangThai] = useState(orderData.trangThai);
    const [dateCreate, setDateCreate] = useState("");

    const dispatch = useDispatch();

    const { modalUpdateOpen } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });
    //Định dạng ngày tháng
    useEffect(() => {
        var d = new Date(parseInt(orderData.ngayTao));

        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var hour = d.getHours();
        var minutes = d.getMinutes();

        var finalDate = year + "-" + month + "-" + day + " " + hour + ":" + minutes;
        setDateCreate(finalDate);
    }, [orderData])

    //mở modal
    const handleModalOpen = () => {
        dispatch(modalUpdateOpenAction())
    };

    //đóng modal
    const handleModalClose = () => {
        dispatch(modalUpdateCloseAction())
    };

    //khi thay đổi trạng thái đơn hàng
    const onChangeStatusOrder = (e) => {
        setTrangThai(e.target.value);
    }

    //Xử lý dữ liệu khi nhấn button thêm
    const handleUpdateOrder = () => {
        var data = {
            trangThai
        }
        if(validateData(data)){
            setValidate(true);
            dispatch(updateOrderByCode(orderData.id, data));
            handleModalClose();
        }else{
            setValidate(false);
        }
    };

    //validate trang thai select
    const validateData = (data)=>{
        if(data.trangThai == ""){
            setMessage("Trạng thái phải được chọn.");
            return false;
        }
        return true;
    };

    return (
        <Modal
            open={modalUpdateOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal} justifyContent="center">
                <Typography component="div" variant="h5" textAlign="center">Edit Order Id: {orderData.id}</Typography>
                {
                    !validate
                        ? <Alert severity="error">{message}</Alert>
                        : <></>
                }
                <Grid container>
                    <Grid item xs={6} my={1} px={1}>
                        <Grid item xs={12} my={1}>
                            <TextField label="Kích cỡ" variant="outlined" value={orderData.kichCo} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Đường kính" variant="outlined" value={orderData.duongKinh} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Sườn" variant="outlined" value={orderData.suon} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Salad" variant="outlined" value={orderData.salad} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Số lượng nước" variant="outlined" value={orderData.soLuongNuoc} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Thành tiền" variant="outlined" value={orderData.thanhTien} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Vourcher" variant="outlined" fullWidth value={orderData.idVourcher} disabled />
                        </Grid>
                    </Grid>
                    <Grid item xs={6} my={1} px={1}>
                        <Grid item xs={12} my={1}>
                            <TextField label="Họ tên *" variant="outlined" fullWidth value={orderData.hoTen} disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Email" variant="outlined" fullWidth value={orderData.email} disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Số điện thoại *" variant="outlined" fullWidth value={orderData.soDienThoai} disabled />

                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Địa chỉ *" variant="outlined" fullWidth value={orderData.diaChi} disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Lời nhắn" variant="outlined" fullWidth value={orderData.loiNhan} disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Loại Pizza" value={orderData.loaiPizza} variant="outlined" fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Loại Đồ uống" value={orderData.idLoaiNuocUong} variant="outlined" fullWidth disabled />
                        </Grid>
                    </Grid>
                    <Grid item xs={12} my={1}>
                        <TextField label="Ngày tạo" value={dateCreate} variant="outlined" fullWidth disabled />
                    </Grid>
                    <Grid item xs={12} my={1}>
                        <FormControl fullWidth>
                            <InputLabel>Trạng thái *</InputLabel>
                            <Select
                                value={orderData.trangThai}
                                label="Trạng thái *"
                                onChange={onChangeStatusOrder}
                            >
                                <MenuItem value="open">Open</MenuItem>
                                <MenuItem value="cancel">Hủy</MenuItem>
                                <MenuItem value="confirmed">Xác nhận</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} my={1}>
                        <Button sx={{ marginRight: 5 }} variant="contained" color="success" onClick={handleUpdateOrder}>Sửa</Button>
                        <Button variant="contained" color="error" onClick={handleModalClose} >Huy</Button>
                    </Grid>
                </Grid>
            </Box >
        </Modal >
    )
};

export default UpdateOrderModal;