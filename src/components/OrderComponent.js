import { Button, Container, Typography, Alert } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalAddOpenAction } from "../action/modal.action";
import { getAllOrder } from "../action/order.action";
import AddOrderModal from "./AddOrderModal";
import ListOrderComponent from "./ListOrderComponent";

const OrderComponent = () => {
    const dispatch = useDispatch();

    const { currentPage, limit, actionChangeStatus, alert } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    })
    //load All Order Api, load lại mỗi khi actionChangeStatus thay đổi
    useEffect(() => {
        dispatch(getAllOrder(currentPage, limit));
    }, [currentPage, actionChangeStatus]);

    //khi click button thêm Order, modal Add được hiện lên
    const onClickAddOrder = () => {
        dispatch(modalAddOpenAction());
    };

    return (
        <Container>
            <Typography variant="h3" component="div" textAlign="Center">Danh sách Orde</Typography>
            {
                alert.status === ""
                    ? <></>
                    : alert.status === false
                        ? <Alert severity="error">{alert.message}</Alert>
                        :<Alert severity="success">{alert.message}</Alert>
            }

            <Button
                variant="contained"
                color="success"
                sx={{ margin: "10px" }}
                onClick={onClickAddOrder}
            >
                Thêm Order
            </Button>
            <ListOrderComponent />
            <AddOrderModal />
        </Container>
    )
}

export default OrderComponent;