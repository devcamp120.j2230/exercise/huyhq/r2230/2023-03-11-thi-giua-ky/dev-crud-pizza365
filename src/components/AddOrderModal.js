import { Button, Modal, Box, Grid, TextField, InputLabel, MenuItem, FormControl, Select, Typography, Alert } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalAddCloseAction, modalAddOpenAction } from "../action/modal.action";
import { createNewOrder } from "../action/order.action";
import { styleModal } from "../styles/Modal.style";

const AddOrderModal = () => {
    const [drinkList, setDrinkList] = useState([]);

    const [validate, setValidate] = useState(false);
    const [message, setMessage] = useState("Nhập thông tin.");

    const [kichCo, setKichCo] = useState("S");
    const [duongKinh, setDuongKinh] = useState(20);
    const [suon, setSuon] = useState(2);
    const [salad, setSalad] = useState(200);
    const [soLuongNuoc, setSoLuongNuoc] = useState(2);
    const [thanhTien, setThanhTien] = useState(150000);

    const [idVourcher, setIdVourcher] = useState("");
    const [hoTen, setHoTen] = useState("");
    const [email, setEmail] = useState("");
    const [soDienThoai, setSoDienThoai] = useState("");
    const [diaChi, setDiaChi] = useState("");
    const [loiNhan, setLoiNhan] = useState("");

    const [idLoaiNuocUong, setIdLoaiNuocUong] = useState("");
    const [loaiPizza, setLoaiPizza] = useState("Seafood");

    //load danh sách đồ uống từ Api
    useEffect(() => {
        return async () => {
            const responseDrink = await fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks")
            const drinkData = await responseDrink.json();
            setDrinkList(drinkData);
        }
    }, [])
    const dispatch = useDispatch();

    const { modalAddOpen } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });

    //mở modal
    const handleModalOpen = () => {
        dispatch(modalAddOpenAction())
    };

    //đóng modal
    const handleModalClose = () => {
        dispatch(modalAddCloseAction())
    };

    //khi chọn kích cỡ Pizza
    const onChangeSizePizza = (e) => {
        setKichCo(e.target.value);
        switch (e.target.value) {
            case "S":
                setDuongKinh(20);
                setSuon(2);
                setSalad(200);
                setSoLuongNuoc(2);
                setThanhTien(150000);
                break;
            case "M":
                setDuongKinh(25);
                setSuon(4);
                setSalad(300);
                setSoLuongNuoc(3);
                setThanhTien(200000);
                break;
            case "L":
                setDuongKinh(30);
                setSuon(8);
                setSalad(500);
                setSoLuongNuoc(4);
                setThanhTien(250000);
                break;
            default:
                break;
        }
    };

    //khi chọn loại Pizza
    const onChangeloaiPizza = (e) => {
        setLoaiPizza(e.target.value);
    };

    //khi chọn loại Đồ uống
    const onChangeidLoaiNuocUong = (e) => {
        setIdLoaiNuocUong(e.target.value);
    };

    //Xử lý dữ liệu khi nhấn button thêm
    const handleAddNewOrder = () => {
        var data = {
            kichCo,
            duongKinh,
            suon,
            salad,
            loaiPizza,
            idVourcher,
            idLoaiNuocUong,
            soLuongNuoc,
            hoTen,
            thanhTien,
            email,
            soDienThoai,
            diaChi,
            loiNhan
        }
        //validate data
        if (validateForm(data)) {
            setValidate(true);
            dispatch(createNewOrder(data));
            handleModalClose();
        } else {
            setValidate(false);
        }

    };

    //validate form
    const validateForm = (data) => {
        //check vourcher
        checkVoucher(data.idVourcher);

        if (data.kichCo === "") {
            setMessage("Hãy chọn Kích cỡ Pizza");
            return false;
        };

        if (data.loaiPizza === "") {
            setMessage("Hãy chọn loại Pizza");
            return false;
        };

        if (data.idLoaiNuocUong === "") {
            setMessage("Hãy chọn loại Đồ uống");
            return false;
        };

        if (data.hoTen === "") {
            setMessage("Họ tên không được để trống.");
            return false;
        };

        if (data.email !== "" && !validateEmail(data.email)) {
            setMessage("Email không đúng định dạng.");
            return false;
        };

        if (data.soDienThoai === "") {
            setMessage("Số điện thoại không được để trống.");
            return false;
        };

        if (data.diaChi === "") {
            setMessage("Địa chỉ không được để trống.");
            return false;
        };

        return true;
    };

    //validate Email
    const validateEmail = (email) => {
        var vRegEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        return email.match(vRegEmail);
    };

    //gửi Api kiểm tra vourcher
    const checkVoucher = async (vourcher) => {
        if (vourcher !== "") {
            const responseDrink = await fetch("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + vourcher)
            const drinkData = await responseDrink.json();
            if (!Number.isInteger(parseInt(vourcher)) || parseInt(drinkData.discount) < 0) {
                setMessage("Vourcher không đúng.");
                setValidate(false);
            } else {
                setValidate(true);
            }
        }
    };

    return (
        <Modal
            open={modalAddOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal} justifyContent="center">
                <Typography component="div" variant="h5" textAlign="center">Thêm mới Order</Typography>
                {
                    !validate
                        ? <Alert severity="error">{message}</Alert>
                        : <></>
                }
                <Grid container>
                    <Grid item xs={6} my={1} px={1}>
                        <Grid item xs={12} my={1}>
                            <FormControl fullWidth>
                                <InputLabel>Kích cỡ *</InputLabel>
                                <Select
                                    value={kichCo}
                                    label="Kích cỡ *"
                                    onChange={onChangeSizePizza}
                                >
                                    <MenuItem value="S">Small</MenuItem>
                                    <MenuItem value="M">Medium</MenuItem>
                                    <MenuItem value="L">Large</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Đường kính" variant="outlined" value={duongKinh} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Sườn" variant="outlined" value={suon} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Salad" variant="outlined" value={salad} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Số lượng nước" variant="outlined" value={soLuongNuoc} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Thành tiền" variant="outlined" value={thanhTien} fullWidth disabled />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Vourcher" variant="outlined" fullWidth onChange={e => setIdVourcher(e.target.value)} />
                        </Grid>
                    </Grid>
                    <Grid item xs={6} my={1} px={1}>
                        <Grid item xs={12} my={1}>
                            {
                                hoTen === ""
                                    ? <TextField label="Họ tên *" variant="outlined" fullWidth onChange={e => setHoTen(e.target.value)} error />
                                    : <TextField label="Họ tên *" variant="outlined" fullWidth onChange={e => setHoTen(e.target.value)} />
                            }
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Email" variant="outlined" fullWidth onChange={e => setEmail(e.target.value)} />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            {
                                soDienThoai === ""
                                    ? <TextField label="Số điện thoại *" variant="outlined" fullWidth onChange={e => setSoDienThoai(e.target.value)} error />
                                    : <TextField label="Số điện thoại *" variant="outlined" fullWidth onChange={e => setSoDienThoai(e.target.value)} />
                            }

                        </Grid>
                        <Grid item xs={12} my={1}>
                            {
                                diaChi === ""
                                    ? <TextField label="Địa chỉ *" variant="outlined" fullWidth onChange={e => setDiaChi(e.target.value)} error />
                                    : <TextField label="Địa chỉ *" variant="outlined" fullWidth onChange={e => setDiaChi(e.target.value)} />
                            }

                        </Grid>
                        <Grid item xs={12} my={1}>
                            <TextField label="Lời nhắn" variant="outlined" fullWidth onChange={e => setLoiNhan(e.target.value)} />
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <FormControl fullWidth>
                                <InputLabel>Loại Pizza *</InputLabel>
                                <Select
                                    value={loaiPizza}
                                    label="Loại Pizza *"
                                    onChange={onChangeloaiPizza}
                                >
                                    <MenuItem value="Seafood">Hải sản</MenuItem>
                                    <MenuItem value="Hawaii">Hawaii</MenuItem>
                                    <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} my={1}>
                            <FormControl fullWidth>
                                <InputLabel>Loại Đồ uống *</InputLabel>
                                <Select
                                    value={idLoaiNuocUong}
                                    label="Loại Đồ uống *"
                                    onChange={onChangeidLoaiNuocUong}
                                >
                                    {drinkList.map((drink, i) => {
                                        return <MenuItem value={drink.maNuocUong} key={i}>{drink.tenNuocUong}</MenuItem>
                                    })}
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} my={1}>
                        <Button sx={{ marginRight: 5 }} variant="contained" color="success" onClick={handleAddNewOrder}>Them</Button>
                        <Button variant="contained" color="error" onClick={handleModalClose} >Huy</Button>
                    </Grid>
                </Grid>
            </Box >
        </Modal >
    )
};

export default AddOrderModal;