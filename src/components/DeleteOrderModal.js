import { Button, Modal, Box, Grid, Typography } from "@mui/material"
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalDeleteCloseAction, modalDeleteOpenAction } from "../action/modal.action";
import { deleteOrderById } from "../action/order.action";
import { styleModal } from "../styles/Modal.style";


const DeleteModal = (props) => {
    const orderId = props.orderId;

    const dispatch = useDispatch();

    const {modalDeleteOpen} = useSelector((reduxData) => {
        return reduxData.orderReducer;
    });

    //handle open modal
    const handleModalOpen = () => {
        dispatch(modalDeleteOpenAction());
    };

    //handle close modal
    const handleModalClose = () => {
        dispatch(modalDeleteCloseAction());
    };

    //handle delete user by id
    const handleDeleteUser = () => {
        handleModalClose();
        dispatch(deleteOrderById(orderId));
    };

    return (
        <Modal
            open={modalDeleteOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container>
                    <Grid item xs={12} my={3}>
                        <Typography variant="h4" component="div" textAlign="left">Confirm Delete User</Typography>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <Typography variant="p" component="div" textAlign="left">Bạn có muốn xóa User (id: {orderId}) này!</Typography>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <Button sx={{ marginRight: 5 }} variant="contained" color="error" onClick={handleDeleteUser}>Xoa</Button>
                        <Button onClick={handleModalClose} variant="contained" sx={{ background: "grey" }}>Huy</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
};

export default DeleteModal;